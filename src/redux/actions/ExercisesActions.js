export const ADD_EXERCISE = 'ADD_EXERCISE'
export const addExercise = ({ name, sets, reps }) => ({
    type: ADD_EXERCISE,
    payload: { name, sets, reps },
})

export const DELETE_EXERCISE = 'DELETE_EXERCISE'
export const deleteExercise = (id) => ({
    type: DELETE_EXERCISE,
    payload: { id },
})

export const RESOLVE_EXERCISE = 'RESOLVE_EXERCISE'
export const resolveExercise = (id) => ({
    type: RESOLVE_EXERCISE,
    payload: { id },
})
