export const selectTheme = (state) => state.theme
export const selectColor = (state) => state.color
export const selectExercises = (state) => state.exercises
