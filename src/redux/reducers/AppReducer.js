import { CHANGE_THEME, CHANGE_COLOR } from '../actions/OptionsActions'
import {
    ADD_EXERCISE,
    DELETE_EXERCISE,
    RESOLVE_EXERCISE,
} from '../actions/ExercisesActions'

const initialState = {
    theme: 'light',
    color: 'blue',
    exercises: [],
}

let lastID = 0

const AppReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CHANGE_THEME:
            return {
                ...state,
                theme: state.theme === 'light' ? 'dark' : 'light',
            }

        case CHANGE_COLOR:
            return {
                ...state,
                color: payload.color,
            }

        case ADD_EXERCISE:
            return {
                ...state,
                exercises: [
                    ...state.exercises,
                    {
                        id: ++lastID,
                        name: payload.name,
                        sets: payload.sets,
                        reps: payload.reps,
                        resolved: false,
                    },
                ],
            }

        case DELETE_EXERCISE:
            return {
                ...state,
                exercises: state.exercises.filter(
                    (exc) => exc.id !== payload.id
                ),
            }

        case RESOLVE_EXERCISE:
            return {
                ...state,
                exercises: state.exercises.map((exc) =>
                    exc.id === payload.id
                        ? { ...exc, resolved: !exc.resolved }
                        : exc
                ),
            }

        default:
            return state
    }
}

export default AppReducer
