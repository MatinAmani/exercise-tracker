import { createTheme } from '@mui/material/styles'
import { blue, green, orange, purple, red, yellow } from '@mui/material/colors'

const Theme = (mode, color) =>
    createTheme({
        palette: {
            mode,
            primary: {
                main:
                    color === 'blue'
                        ? blue[500]
                        : color === 'red'
                        ? red[500]
                        : color === 'orange'
                        ? orange[500]
                        : color === 'yellow'
                        ? yellow[500]
                        : color === 'purple'
                        ? purple[500]
                        : green[500],
            },
        },
    })

export default Theme
