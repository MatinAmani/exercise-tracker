import { Card } from '@mui/material'
import { makeStyles } from '@mui/styles'

import Options from './sections/Options'
import AddItem from './sections/AddItem'
import Items from './sections/Items'

const useStyles = makeStyles((theme) => ({
    container: {
        width: '60vw',
        [theme.breakpoints.down('md')]: {
            width: '80vw',
        },
        margin: theme.spacing(4),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'stretch',
    },
}))

const Main = () => {
    const classes = useStyles()

    return (
        <Card className={classes.container} raised>
            <Options />
            <AddItem />
            <Items />
        </Card>
    )
}

export default Main
