import { TextField, MenuItem } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { useDispatch, useSelector } from 'react-redux'
import { selectTheme, selectColor } from '../../redux/selectors'
import { changeTheme, changeColor } from '../../redux/actions/OptionsActions'

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: theme.spacing(2, 0),
    },
    options: {
        margin: theme.spacing(0, 2),
        width: '25vw',
        [theme.breakpoints.down('md')]: {
            width: '35vw',
        },
    },
}))

const colors = ['blue', 'red', 'orange', 'yellow', 'purple', 'green']

const Options = () => {
    const classes = useStyles()

    const dispatch = useDispatch()

    const theme = useSelector(selectTheme)
    const color = useSelector(selectColor)

    const handleThemeChange = () => dispatch(changeTheme())
    const handleColorChange = ({ target }) =>
        dispatch(changeColor(target.value))

    return (
        <div className={classes.container}>
            <TextField
                className={classes.options}
                select
                value={theme}
                label="Select Theme"
                onChange={handleThemeChange}
            >
                <MenuItem key="light" value="light">
                    Light
                </MenuItem>
                <MenuItem key="dark" value="dark">
                    Dark
                </MenuItem>
            </TextField>
            <TextField
                className={classes.options}
                select
                value={color}
                label="Select Color"
                onChange={handleColorChange}
            >
                {colors.map((clr) => (
                    <MenuItem key={clr} value={clr}>
                        {clr.toUpperCase()}
                    </MenuItem>
                ))}
            </TextField>
        </div>
    )
}

export default Options
