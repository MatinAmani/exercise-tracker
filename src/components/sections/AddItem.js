import { TextField, Button, MenuItem, Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { useFormik } from 'formik'
import { object, string, number } from 'yup'
import { useDispatch } from 'react-redux'
import { addExercise } from '../../redux/actions/ExercisesActions'

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        [theme.breakpoints.down('md')]: {
            alignItems: 'center',
        },
    },
    inputsContainer: {
        margin: theme.spacing(1, 0),
    },
}))

const AddItem = () => {
    const classes = useStyles()

    const dispatch = useDispatch()

    const onSubmit = (values, actions) => {
        dispatch(addExercise(values))
        actions.resetForm()
    }

    const validationSchema = object().shape({
        name: string().required('Required'),
        sets: number().required('Required'),
        reps: number().required('Required'),
    })

    const formik = useFormik({
        initialValues: {
            name: '',
            sets: '',
            reps: '',
        },
        onSubmit,
        validationSchema,
    })

    return (
        <form className={classes.container} onSubmit={formik.handleSubmit}>
            <Grid className={classes.inputsContainer} container spacing={2}>
                <Grid item xs={12} md={8}>
                    <MyInput
                        name="name"
                        label="Enter the name of the exercise"
                        formik={formik}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6} md={2}>
                    <MyInput
                        name="sets"
                        label="Sets"
                        formik={formik}
                        fullWidth
                        options={[1, 2, 3, 4, 5]}
                    />
                </Grid>
                <Grid item xs={6} md={2}>
                    <MyInput
                        name="reps"
                        label="Reps"
                        formik={formik}
                        fullWidth
                        options={[5, 7, 8, 10, 12, 15, 20, 25, 40]}
                    />
                </Grid>
            </Grid>
            <Button type="submit" variant="contained" color="primary">
                Add
            </Button>
        </form>
    )
}

const MyInput = ({ name, formik, options = null, ...props }) => {
    return (
        <TextField
            name={name}
            onChange={formik.handleChange}
            error={formik.touched[name] && formik.errors[name] ? true : false}
            helperText={
                formik.touched[name] && formik.errors[name]
                    ? formik.errors[name]
                    : null
            }
            value={formik.values[name]}
            select={options ? true : false}
            {...props}
        >
            {options &&
                options.map((opt) => (
                    <MenuItem key={opt} value={opt}>
                        {opt}
                    </MenuItem>
                ))}
        </TextField>
    )
}

export default AddItem
