import { useSelector, useDispatch } from 'react-redux'
import { selectExercises } from '../../redux/selectors'
import {
    resolveExercise,
    deleteExercise,
} from '../../redux/actions/ExercisesActions'
import {
    List,
    ListItemButton,
    ListItemText,
    ListItemSecondaryAction,
    IconButton,
} from '@mui/material'
import { makeStyles } from '@mui/styles'
import CheckCircleOutlineRounded from '@mui/icons-material/CheckCircleOutlineRounded'
import CheckCircleRounded from '@mui/icons-material/CheckCircleRounded'
import DeleteRounded from '@mui/icons-material/DeleteRounded'

const useStyles = makeStyles((theme) => ({
    itemContainer: {
        // ignored
    },
}))

const Items = () => {
    const items = useSelector(selectExercises)

    return (
        <List>
            {items.map((item) => (
                <MyItem key={item.id} item={item} />
            ))}
        </List>
    )
}

const MyItem = ({ item }) => {
    const classes = useStyles()

    const dispatch = useDispatch()

    const handleResolve = (id) => dispatch(resolveExercise(id))
    const handleDelete = (id) => dispatch(deleteExercise(id))

    return (
        <ListItemButton
            className={classes.itemContainer}
            onClick={() => handleResolve(item.id)}
        >
            <ListItemText
                primary={item.name}
                secondary={
                    item.sets > 1
                        ? `${item.sets} Sets - ${item.reps} Reps`
                        : `${item.sets} Set - ${item.reps} Reps`
                }
            />
            <ListItemSecondaryAction>
                <IconButton color="primary">
                    {item.resolved ? (
                        <CheckCircleRounded />
                    ) : (
                        <CheckCircleOutlineRounded />
                    )}
                </IconButton>
                <IconButton onClick={() => handleDelete(item.id)}>
                    <DeleteRounded color="error" />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItemButton>
    )
}

export default Items
