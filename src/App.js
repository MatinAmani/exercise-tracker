import { ThemeProvider } from '@mui/material/styles'
import Theme from './themes/main'
import { CssBaseline } from '@mui/material'
import { useSelector } from 'react-redux'
import { selectTheme, selectColor } from './redux/selectors'

import Main from './components/Main'

const App = () => {
    const mode = useSelector(selectTheme)
    const color = useSelector(selectColor)

    return (
        <ThemeProvider theme={Theme(mode, color)}>
            <CssBaseline />
            <Main />
        </ThemeProvider>
    )
}

export default App
